'use strict';

/**
 * Run the animation functions.
 */
Banner.prototype.start = function () {
  this.banner = document.querySelector('.banner');

  this.bannerWidth = this.banner.offsetWidth;
  this.bannerHeight = this.banner.offsetHeight;

  // Image array for preloading
  this.images = [
    'images/logo.png',
    'images/logo-big.png',    
    'images/bg.jpg',
    'images/copy1.png',
    'images/copy2.png',
    'images/copy3.png',
    'images/copy4.png',
    'images/copy5.png',
    'images/copy6.png',
    'images/copy7.png',
    'images/copy8.png',
    'images/copy9.png',
    'images/copy10.png',
    'images/copy11.png',            
    'images/cta-nrm.png',
    'images/cta-ovr.png',    
    'images/tag.png',
    // 'images/vig.png',    
  ];

  var _this = this;
  this.preloadImages(this.images, function () {
    _this.createElements();
    _this.setup();
    _this.hidePreloader();
    _this.animate();
  });
};

/**
 * Create dom elements.
 */
Banner.prototype.createElements = function () {
  this.bg = this.smartObject({
    id: 'bg',
    backgroundImage: 'images/bg.jpg',
    top: 'none',
    left: 'none',
    parent: this.banner
  });

  this.logo = this.smartObject({
    id: 'LOGO',
    backgroundImage: 'images/logo.png',
    parent: this.banner
  });

  this.logoBig = this.smartObject({
    id: 'LOGO-BIG',
    backgroundImage: 'images/logo-big.png',
    parent: this.banner
  });    

  this.copy1 = this.smartObject({
    id: 'copy1',
    backgroundImage: 'images/copy1.png',
    parent: this.banner
  });

  this.copy2 = this.smartObject({
    id: 'copy2',
    backgroundImage: 'images/copy2.png',
    parent: this.banner
  });      

  this.copy3 = this.smartObject({
    id: 'copy3',
    backgroundImage: 'images/copy3.png',
    parent: this.banner
  });

  this.copy4 = this.smartObject({
    id: 'copy4',
    backgroundImage: 'images/copy4.png',
    parent: this.banner
  });

  this.copy5 = this.smartObject({
    id: 'copy5',
    backgroundImage: 'images/copy5.png',
    parent: this.banner
  });

  this.copy6 = this.smartObject({
    id: 'copy6',
    backgroundImage: 'images/copy6.png',
    parent: this.banner
  });

  this.copy7 = this.smartObject({
    id: 'copy7',
    backgroundImage: 'images/copy7.png',
    parent: this.banner
  });

  this.copy8 = this.smartObject({
    id: 'copy8',
    backgroundImage: 'images/copy8.png',
    parent: this.banner
  });

  this.copy9 = this.smartObject({
    id: 'copy9',
    backgroundImage: 'images/copy9.png',
    parent: this.banner
  });

  this.copy10 = this.smartObject({
    id: 'copy10',
    backgroundImage: 'images/copy10.png',
    parent: this.banner
  }); 

  this.copy11 = this.smartObject({
    id: 'copy11',
    backgroundImage: 'images/copy11.png',
    parent: this.banner
  });      

  this.tag = this.smartObject({
    id: 'tag',
    backgroundImage: 'images/tag.png',
    parent: this.banner
  });

  this.cta = this.smartObject({
    id: 'cta',
        backgroundImage: 'images/cta-nrm.png',
    parent: this.banner
  });

      this.ctaNrm2 = this.smartObject({
        id: 'ctaNrm2',
        backgroundImage: 'images/cta-nrm.png',
        parent: this.cta
      });

      this.ctaOvr2 = this.smartObject({
        id: 'ctaOvr2',
        backgroundImage: 'images/cta-ovr.png',
        parent: this.cta
      });   

  this.cta2 = this.smartObject({
    id: 'cta',
    width: 118,
    height: 27,
    parent: this.banner
  });

      this.ctaNrm = this.smartObject({
        id: 'ctaNrm',
        backgroundImage: 'images/cta-nrm.png',
        parent: this.cta2
      });

      this.ctaOvr = this.smartObject({
        id: 'ctaOvr',
        backgroundImage: 'images/cta-ovr.png',
        parent: this.cta2
      });  
};

/**
 * Setup initial element states.
 */
Banner.prototype.setup = function () {
  this.copy1.set({ left: -10, top: 65 });
  this.copy2.set({ left: -10, top: 105 });
  this.copy3.set({ left: -10, top: 145 });
  this.copy4.set({ left: -10, top: 65 });
  this.copy5.set({ left: -10, top: 105 });
  this.copy6.set({ left: -10, top: 145 });
  this.copy7.set({ left: -10, top: 65 });
  this.copy8.set({ left: -10, top: 105 });
  this.copy9.set({ left: -10, top: 145 }); 
  this.copy10.set({ left: -10, top: 65 }); 
  this.copy11.set({ left: -10, top: 105 });          
  this.logo.set({ left: 16, top: 564 });
  this.logoBig.centerHorizontal();
  this.logoBig.set({ top: 554 });
  this.tag.centerHorizontal();  
  this.tag.set({ top: 510 });
  this.cta.set({ left: 169, top: 557 });
    this.ctaOvr2.set({ autoAlpha: 0 });
  this.cta2.set({ left: 14, top: 153 });  
    this.ctaOvr.set({ autoAlpha: 0 });  
  this.bg.set({ right: 0, bottom: 0 });
};

/**
 * Hide the preloader.
 */
Banner.prototype.hidePreloader = function () {
  TweenLite.to('.preloader', 1, { autoAlpha: 0 });
};

/**
 * Animation timeline.
 */
Banner.prototype.animate = function () {
  var _this = this;
  var loop = 0;

  // this.timeLineCTA = new TimelineMax({ paused: true })
  //   .to(this.ctaOvr, 1, { autoAlpha: 1 }, 'cta');

  this.cta2.addEventListener('mouseover', () => {
    TweenLite.to(this.ctaOvr, 1, { autoAlpha: 1 });
  });

  this.cta2.addEventListener('mouseout', () => {
    TweenLite.to(this.ctaOvr, 1, { autoAlpha: 0 });
  });

  this.cta.addEventListener('mouseover', () => {
    TweenLite.to(this.ctaOvr2, 1, { autoAlpha: 1 });
  });

  this.cta.addEventListener('mouseout', () => {
    TweenLite.to(this.ctaOvr2, 1, { autoAlpha: 0 });
  });

  this.copyAnimation = new TimelineMax({paused: true})
    .staggerFrom([this.copy1, this.copy2, this.copy3], 0.4, { x: -300, ease: Power1.easeOut }, 0.1)
    .to([this.copy1, this.copy2, this.copy3], 0.4, { x: -300, ease: Power1.easeIn }, '+=3')
    .staggerFrom([this.copy4, this.copy5, this.copy6], 0.4, { x: -300, ease: Power1.easeOut }, 0.1)
    .to([this.copy4, this.copy5, this.copy6], 0.4, { x: -300, ease: Power1.easeIn }, '=+3')
    .staggerFrom([this.copy7, this.copy8, this.copy9], 0.4, { x: -300, ease: Power1.easeOut }, 0.1, 'cta')
    // .from(this.cta, 1, { autoAlpha: 0 }, 'cta')
    .to([this.copy7, this.copy8, this.copy9], 0.4, { x: -300, ease: Power1.easeIn }, '=+2')
    .to([this.logo, this.cta], 0.4, { autoAlpha: 0 }, 'swap')
    .from([this.logoBig, this.tag], 0.5, { autoAlpha: 0 }, 'swap+=0.5')      
    // .staggerFrom([this.copy10, this.copy11, this.cta2], 0.4, { autoAlpha: 0 }, 0.4, 'swap+=1')
    .staggerFrom([this.copy10, this.copy11], 0.4, { x: -300, ease: Power1.easeOut }, 0.1)
    .from(this.cta2, 0.4, { autoAlpha: 0 }) 
    .call(function(){
        loop++
        console.log('loop: ', loop);
        if (loop >= 2){
          console.log('if')
          _this.copyAnimation.addPause('reset', _this.addEventListeners, [_this]);
        } else {
          console.log('else')
          _this.copyAnimation.to([this.copy10, this.copy11, this.logoBig, this.tag, this.cta2], 0.5, { autoAlpha: 0 })
        }
    });


  this.timeline = new TimelineMax({paused: false, repeat: 1, repeatDelay: 1})
    .add(this.copyAnimation.play(), 'frame2+=1')
    .from(this.bg, 13, { x: 26, easing:Linear.easeNone , rotation:0.01 }, 'frame2+=1');

  this.timeline.timeScale(1);
};

Banner.prototype.addEventListeners = function (_this) {
  // var _this = this;
  console.log(_this);

  _this.banner.addEventListener( 'mouseover', rollover );
  _this.banner.addEventListener( 'mouseout', rollout );

  function rollover() {
    _this.timeLineCTA.play();
  }

  function rollout() {
    _this.timeLineCTA.reverse();
    _this.timeLineCTA.timeScale(2);
  }
};
